# Gamma irradiation facility (GIF++)

This page contains useful informations on the GIF++ test beams carried by the GEM collaboration.

## GEM test beams at GIF++

This section contains a list of the test beams performed at GIF++ and their results.

### ME0 test beam - July 2022

**Setup**: 1 ME0 detector prototype (final design).

**Results**:

- [ME0 GIF++ test beam results](https://indico.cern.ch/event/1223613/#25-update-on-gif-test-beam-mea) in GEM Phase-2 R&D meeting
- [ME0 rate capability](https://indico.cern.ch/event/1213272/contributions/5143403/) in 32nd GEM workshop
- [ME0 rate capability and dead time update](https://indico.cern.ch/event/1236355/#126-update-on-vfat-rate-capabi) in GEM Phase-2 Electronics Meeting

### ME0 test beam - July 2023 

**Setup**: 1 ME0 4-layer stack prototype.

**Results**:

- [Status of July 2023 GIF++ test beam analysis](https://indico.cern.ch/event/1371324/#5-status-of-july-2023-gif-test) in ME0 studies and test beam meeting
- [CMS GEM report](https://indico.cern.ch/event/1343930/#sc-6-29-cms-gem) in 7th Annual GIF++ User Meeting 2023

## Retrieving GIF++ source trends using Timber

[Timber](https://timber.cern.ch) provides access to the CERN Accelerator Logging service data, including historical data of the GIF++ source status (ON/OFF) and its attenuation values.

!!! note
    Timber can only be accessed inside the CERN network. To access Timber outside CERN, [set up a SSH tunnel](/other/proxy) first.

To retrieve the source data on Timber follow these steps:

1. in the Timber *Query* page, open the *Hierarchies* tab;
2. ensure the *System* dropdown menu is set to *CERN* and navigate to *MCS* > *GIFpp* in the layout tree;
3. navigate to *Attenuators* > *DownStream*, then in the variable table enable the checkbox for `DOWNSTREAM_EFFECTIVE_ATTENUATION`;
4. repeat step 3 for the following variables:
    - `UPSTREAM_EFFECTIVE_ATTENUATION` in *MCS* > *GIFpp* > *Attenuators* > *UpStream*;
    - `SOURCE_OFF` in *MCS* > *GIFpp* > *Irradiator*;
    - `SOURCE_ON` in *MCS* > *GIFpp* > *Irradiator*;
5. in the *Current configuration* panel on the right, scroll to the *Time selection* section and click on the tab *Fixed*;
6. select the desired start and stop dates and times;
7. click on *Load* to show the trends in the *Visualization* tab.

To download the data in csv:

1. in the *Current configuration* panel scroll down to the *Output* section, then select the *File* tab;
2. enable the checkbox *Group by timestamp*;
3. click on *Load*.

## HV compensation for GEM detectors
When GEM detectors are working in a high background rate environment, like at GIF++, the multitude of electrons moving through the electrodes can generate a not-negigible amount of current flowing through the protection resistors. Consequently, it causes a drop of the voltage applied to the foils , and, finally, a decrease of the gain. To recover this loss of gain a HV compensation process has been designed. The following flowchart displays the procedure used for the HV compensation of one of the 7 electrodes of a triple-GEM detector and it is applied identically to each of them.

```mermaid
graph TB
  A["Initial state (i=0)"] --> B["V<sub>SET</sub><sup>i</sup> = V<sub>NOM</sub>"];
  B --> C["The current, I<sub>MONDET</sub><sup>i</sup>, through the R<sub>PROT</sub> is measured by the HV power supply"]
  C --> D["The voltage drop is computed: I<sub>MONDET</sub><sup>i</sup> x R<sub>PROT</sub>"]
  D --> E["The effective voltage at the i-th step is V<sub>EFF</sub><sup>i</sup> = V<sub>SET</sub><sup>i</sup> + I<sub>MONDET</sub><sup>i</sup> x R<sub>PROT</sub>"]
  E --> F{" |V<sub>EFF</sub><sup>i</sup> - V<sub>NOM</sub>| < V<sub>TOLERANCE</sub> <sup>(1)</sup>"}
  F -->|Yes| L["Convergence reached: V<sub>APP</sub> = V<sub>SET</sub><sup>i</sup>"];
  F ---->|No| D1["i=i+1"];
  D1 --> H["The voltage applied is set to V<sub>SET</sub><sup>i</sup> = V<sub>SET</sub><sup>i-1</sup> + I<sub>MONDET</sub><sup>i-1</sup> x R<sub>PROT</sub>"]
  H --> C
```

(1) The parameter V<sub>TOLERANCE</sub> can be varied depending on the required degree of tolerance. A reasonable value is 1V. 

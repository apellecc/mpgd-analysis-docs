# Frequently asked questions

## Compiling

### Could NOT find ZSTD (missing: ZSTD_LIBRARY ZSTD_INCLUDE_DIR)

If `cmake` is giving you this error, a `zstd` installation is missing from your environment. We use `zstd` only to unpack compressed files containing raw GEM data, so if you do not need the GEM unpacker (e.g. you are only running on rechits, or you are running on SRS data), build the project excluding the `zstd` dependency.

```bash
cmake3 -DUSE_ZSTD=OFF ..
make -j16
```

Of course in this case the GEM unpacker will not be compiled.

See the [build instructions](/guide/compile/#zstd) for more informations.

## Analysis

### ImportError: urllib3 v2.0 only supports OpenSSL 1.1.1+, currently the 'ssl' module is compiled with LibreSSL x.y.z

If you get a similar error, you are using an OS version with an outdated OpenSSL version (for example Centos 7). If you cannot upgrade OpenSSL with your OS package manager, downgrade your `urllib3` version instead:

```bash
pip install urllib3==1.26.6
```
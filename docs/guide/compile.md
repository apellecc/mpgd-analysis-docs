# Developer guide

If you want to contribute to the code or you want to run on a private machine you probably need to compile it from source. The steps to compile and install the reconstruction and analysis packages are explained separately below.
This page assumes you have already read and tried out the [user guide](/guide/user).

## How to build the reconstruction code

### Requirements

#### ROOT

ROOT is required for file I/O only. [Install it](https://root.cern/install/) from your package manager, [compile it](https://root.cern/install/build_from_source/) from source or [enable a preinstalled environment](https://root.cern/install/#lcg-releases-on-cvmfs).

#### zstd

The `zstd` library is only needed by the GEM unpacker to reconstruct compressed raw data. Therefore you only need it if you verify **both** these conditions:

- you are running on data using CMS GEM electronics;
- you need to reconstruct starting from raw data.

If you verify both these requirements, you can obtain the `zstd` library in one of these possible ways:

- if you are running on a private machine, install the package `libzstd-devel` using your package manager;
- if you are running on Centosstream9 or an equivalent system (e.g. on lxplus9) and you have eos access, add the following command line argument to `cmake` in the build instructions in [the next paragraph](/guide/compile/#compiling):

    ```cmake
    -DZSTD_PATH=/eos/project/m/mpgd-analysis/public/dependencies/zstd/el9/install
    ```

- (**advanced**) if you are running on a private machine and can't install software using your package manager, compile `zstd` [from source](https://github.com/facebook/zstd) and set the cmake `ZSTD_PATH` variable appropriately.

### Compiling

To compile the reconstruction code:

1. clone the repository:

    ```bash
    git clone https://gitlab.cern.ch/apellecc/mpgd-analysis.git
    cd mpgd-analysis
    ```

2. configure `cmake`:

    ```bash
    cmake3 -S . -B build -DCMAKE_INSTALL_PREFIX=$PWD/build/install
    ```

    If you don't have ZSTD installed and you don't need it (see [previous paragraph](/guide/compile/#zstd)), add the option `-DUSE_ZSTD=OFF`.
    If you don't have ZSTD and you have access to eos, add the option `-DZSTD_PATH=/eos/project/m/mpgd-analysis/public/dependencies/zstd/el9/install`.

    Change the `-DCMAKE_INSTALL_PREFIX` parameter to install the executables to any other folder of your choice.

    !!! note
        Hint: run `cmake` with the `CMAKE_CXX_FLAGS=-Ox` (x = 1, 2, 3) option to enable compiler optimization at level `x`.

3. compile and install the code:

    ```bash
    cmake3 --build build --target install
    ```

## Analysis software

The analysis software does not need to be compiled and can be installed like any [local Python package](https://packaging.python.org/en/latest/tutorials/installing-packages/).

### Requirements

A Python version of 3.8 or higher is needed; if you are running on Centos 7 you can enable it using
```bash
scl enable rh-python38 bash
```

### Installation

To install the analysis package, it is recommended to use a virtual environment:

1. create and activate a virtual environment:
    ```bash
    python3 -m venv mpgd-analysis-env
    source mpgd-analysis-env/bin/activate
    python3 -m pip install --upgrade pip
    ```

2. clone the repository (if you haven't done so already):

    ```bash
    git clone https://gitlab.cern.ch/apellecc/mpgd-analysis.git
    cd mpgd-analysis
    ```

3. install the analysis package and its dependencies in editable mode:
    ```bash
    python3 -m pip install --editable analysis/
    ```

    The editable mode ensures any changes you do in the code during your development are available to the `mpgd-analysis` executable.

## Usage in a fresh session

From now on, in order to enable the virtual environment needed by the analysis package and point to the location where your geometry files are stored, in each new session you need to execute these commands:

```bash
source mpgd-analysis-env/bin/activate
export MPGD_ANALYSIS_HOME=/full/path/to/mpgd-analysis/repository/folder
```

In case you do not need to run the analysis (but only the reconstruction code), you can omit the first command.
# User guide

This page describes the steps to run the reconstruction and analysis code on lxplus without performing a full build. If you are trying to compile the software on an unsupported or private machine, read the [Development guide](/guide/compile) instead.

The following instructions rely on the latest version of the software in its `main` branch, available in the folder
```bash
/eos/project/m/mpgd-analysis/public/releases/latest
```

The only officially supported platform at the moment is **lxplus9** (called by its codename `el9`).

## Reconstruction

To set the necessary environment in your lxplus user space run the following command at the beginning of each session:
```bash
source /eos/project/m/mpgd-analysis/public/setups/el9/setup.sh
```

That's it! Read further to check you can run the reconstruction successfully.

### Reconstructing an example run

To check that you can run the reconstruction successfully, execute the following commands to reconstruct an example test beam run of a stack made of four ME0 detectors. Check the output of each command to be sure you are not getting any errors.

```bash
mkdir mpgd-analysis-test && cd mpgd-analysis-test
wget https://apellecc.web.cern.ch/apellecc/software/mpgd-analysis/test/me0/109.raw.zst
raw-to-digi --input 109.raw.zst --output digi.root --geometry stack-tb-april2023
digi-to-rechit digi.root rechit.root --geometry stack-tb-april2023
rechit-to-track rechit.root track.root --geometry stack-tb-april2023
```

!!! note
    Read the [Reconstruction guide](/reconstruction/reco) for informations about the reconstruction steps performed by each command.

You should find the runs at the various reconstruction steps in the output folder:

```bash
[MY-USERNAME@lxplus mpgd-analysis-test]$ ls
109.raw.zst  digi.root  rechit.root  track.root
```

### How to use a custom mapping and geometry folder

If you are working on a new setup, chances are you want to run the code with a custom geometry or mapping. To modify an existing geometry (or mapping or channel mask) or write a new one, copy the `mapping`, `geometry` and `masks` folders to your local user folder:

```bash
mkdir my-new-setup
cp -r /eos/project/m/mpgd-analysis/public/releases/latest/{mapping,geometry,masks} my-new-setup/
```

Do your modifications to the geometry in the new folder, then set the `MPGD_ANALYSIS_HOME` environment variable to the path where you stored the new geometry folders:
```bash
export MPGD_ANALYSIS_HOME=/full/path/to/my-new-setup
```

You can now run the reconstruction as usual and the geometry files will be picked from your custom folder.

## Analysis

The analysis package should be installed in your local user folder to be run. It is advised to do so in a virtual environment.

For the installation refer to the [developer guide](/guide/compile/#analysis).

### Usage

From now on, run this command in each new session to enable the analysis environment:

```bash
source mpgd-analysis-env/bin/activate
source /eos/project/m/mpgd-analysis/public/setups/el9/setup.sh
```

!!! note
    The last command is only needed to set the correct `MPGD_ANALYSIS_HOME` environment variable and point to your geometry files. You need to set a custom value of `MPGD_ANALYSIS_HOME` if you are storing your geometry files elsewhere, as explained in the [previous paragraph](/guide/user/#how-to-use-a-custom-mapping-and-geometry-folder).

### Analyzing an example run

To test the analysis software you can analyze the track file of the example run you reconstructed in the [previous section](/guide/user#reconstructing-an-example-run):

```bash
cd /mpgd-analysis-test # or wherever you stored the example track file
mpgd-analysis me0 tracks track.root results --efficiency --fit
```

The `results` folder contains the following folders:

- `residuals` contains the residual plot for each detector and each eta partition;
- `matching-profiles` contains the distribution of all the propagated hits and matching propagated hits on each detector and eta partition;
- `efficiency-profiles` contains efficiency distributions - calculated as the ratio between the matching prophits and the total prophits - for each detector and eta partition;
- `efficiency-maps` contains coarse-binned efficiency maps for all detectors.

The file `efficiency.csv` contains the list of matching and prophits for each detector and readout partition (useful for example for grouping several runs or analyzing multiple runs in a parameter scan).

Now go ahead and analyze several runs in your setup. Run the command `mpgd-analysis --help` to know which setups are presently supported by the analysis; if your setup is unsupported and you need to write the analysis for it, consider creating a [merge request](https://gitlab.cern.ch/apellecc/mpgd-analysis/-/merge_requests) on the mpgd-analysis repository to contribute to keeping the code up-to-date.
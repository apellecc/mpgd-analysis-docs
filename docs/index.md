# Introduction

The `mpgd-analysis` is a repository containing:

* a set of reconstruction tools for MPGD data, written in C++;
* an analysis package for reconstructed data, written in Python.

## How does the reconstruction and analysis work?

Documentation on the concepts is at the moment sparse. For the most up-to-date informations refer to the [Reconstruction section](/reconstruction/reco). More (outdated) ideas and todos are explained in [this set of slides](https://docs.google.com/presentation/d/1p60Ddj4Xe4NQAYWLp28O207NohF_f0jMai_7c54Ymxs/edit?usp=sharing).

## Quick start

If you just want to quickly run the code on lxplus, read the [User guide](/guide/user).

## Help!

If you are panicking, check out the [Frequently asked questions](docs/reconstruction/subchapter1/clustering.md) section - chances are somebody already met your problem.

## Other resources

If you are working with `mpgd-analysis`, you probably need the informations contained in at least one of the following sites:

- **[CMS GEM documentation](https://cmsgemonline.web.cern.ch/)**: contains useful documentation about the CMS GEM electronics, DAQ and operations;
- **[CMS GEM EDMS space](https://edms.cern.ch/ui/#!master/navigator/project?P:1424611140:1424611140:subDocs)**: contains technical specifications about the CMS GEM detector mechanics;
- **[VMM3a/SRS documentation](https://vmm-srs.docs.cern.ch/)**: documentation about the VMM ASIC DAQ and operation using the SRS backend.
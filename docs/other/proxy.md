# How to access CERN-restricted webpages from an external network

Follow this guide to redirect web traffic for CERN webpages through Firefox using SSH dynamic port forwarding.
Inspired and generalized from the documentation page for Mac OS on [Accessing Internal Webpages from Outside CERN](https://devices.docs.cern.ch/devices/mac/sshTunnel/){:target="_blank"}.
Supports:

- operating systems: Windows, Linux, Mac OS;
- browsers: Firefox.

## How to set up and start a proxy session

1. Create a file named `cern.pac` in your home directory (e.g. `C:/Users/MyUser/Documents/`) and paste in the following content:

    ```javascript
    function FindProxyForURL(url, host) {
    if (shExpMatch(url,"*.cern.ch*")) {
    return "SOCKS5 127.0.0.1:8081";

    }
    // All other requests go directly to the 'Net':
    return "DIRECT";
    }
    ```

2. In Firefox:

    - open the menu and click on *Settings*;
    - scroll to *Network Settings* and click on *Settings*;
    - enable the choice "Automatic proxy configuration (URL)";
    - enter in the text field the full path of the `cern.pac` file, prepended by the string `file:///` (e.g. `file:///C:/Users/MyUser/Documents/cern.pac`);
    - click on *Ok*.

3. Open an ssh connection to lxplus with dynamic forwarding on the port 8081:

    === "Linux/Mac OS"

        Open a terminal and run the command
        ```bash
        ssh -D 8081 YOUR-CERN-USERNAME@lxplus.cern.ch
        ```

    === "Windows"

        Procedure adapted from blog post [How to use PuTTY as a socks proxy](https://www.pwndefend.com/2022/06/25/how-to-use-putty-as-a-socks-proxy/){:target="_blank"} (I have tested it on Windows 10):

        - download and install PuTTY from the [PuTTY website](https://www.chiark.greenend.org.uk/~sgtatham/putty/);
        - open PuTTY and in the *Session* menu type `YOUR-CERN-USERNAME@lxplus.cern.ch` as hostname;
        - in the *Category* panel navigate to *Connection* > *SSH* > *Tunnels*
        - enable the **two** checkboxes "Local ports accept connections from other hosts" and "Remote ports do the same (SSH-2 only)";
        - set the *Source port* field to 8081 and leave the *Destination* field empty;
        - select the *Dynamic* option;
        - click on *Add*;
        - optional: in order to save this configuration, go back to the *Session* menu, type `lxplus-tunnel` in the *Saved sessions* field and click on *Save*;
        - click on *Open* to start the SSH connection;
        - enter your CERN password.

To verify that the procedure has worked, try opening on Firefox a website accessible from the CERN network only, such as [Timber](http://timber.cern.ch){:target="_blank"}.
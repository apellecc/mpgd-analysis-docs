# Clustering and local reconstruction

Local reconstruction converts digi events to *rechits*, i.e. hits with a position and size in the physical space. It consists of two steps:

- **clustering**: groups of neighbouring readout elements (strips or pads) are grouped together in clusters[^1];
- **geometry mapping**: each cluster is assigned a physical position (in mm) in the detector plane.

[^1]: Pad clusters are built with stricted requirements on charge sharing with respect to strip clusters.

Each rechit is characterized by:

- a center, equal to the geometrical (for digital readout, e.g. VFAT) or weighted (for analog redout, e.g. APV) center of the cluster;
- a cluster size, equal to the distance between the first and last readout element of the cluster[^2];
- a charge, equal to the total charge of all the digis in the cluster; 
- an arrival time, equal to the average arrival times of the component digis[^3].

[^2]: Pad clusters have a different cluster size in the x and y directions.
[^3]: Available only in setups with timing information (e.g. APV) or in multi-BX runs.

Masked readout elements are not included in the cluster reconstruction.

![Strip and pad clustering](/static/images/strip_cluster.png)

## Clustering algorithm

Clusters are built by choosing "seed" clusters made of single digis and then gradually grown by including all the neighbouring digis found by iterating on all the digis in the same readout partition.

```mermaid
graph TB
A[Pick first digi in the list] --> B[Define seed cluster of size 1];
B --> C[Scan digis in detector];
C --> D{Is the digi neighbour to the cluster?};
D --> |Yes| E[Add the digi to the cluster and remove it from the digi list];
E --> C;
D --> |No| F{Are there more digis to scan in the detector?};
F --> |Yes| C;
F --> |No| G{Is the list of digis empty?};
G --> |No| A;
G --> |Yes| H[End];
```

## How to run

```bash
digi-to-rechit DIGI-FILE-NAME.root RECHIT-FILE-NAME.root --geometry GEOMETRY-NAME 
```
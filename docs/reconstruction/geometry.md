# Mapping and geometry

The mapping and geometry informations are stored in text files and are needed by all reconstruction steps.

## Mapping

The mapping data are stored in the `mapping` folder. Each subfolder in `mapping` indicates a different setup.

Within each setup folder `mapping/SETUP-NAME` you will find:

- a setup mapping file called `mapping.csv`; this file maps the each chip (APV or VFAT) to the corresponding chamber;
- one or more detector mapping files, that map each chip ID and channel number to a readout element (identified by eta partition and strip or pad number).

Example of setup mapping file (`mapping.csv`) for the GE2/1 QC8:
```
fed,slot,oh,vfat,chamber
11,0,0,0,0
11,0,0,1,0
11,0,0,2,0
11,0,0,3,0
...
```

Example of detector mapping (`ge21module.csv`) for the GE2/1 QC8:
```
vfat,channel,eta,strip
2,0,4,320
2,1,3,320
2,2,4,319
2,3,3,319
...
```

## Geometry

The geometry files are stored in the `geometry` folder. The geometry of each setup is described by the file `geometry/setups/SETUP-NAME.xml`.

!!! Note
    For a given setup, the `SETUP-NAME` string must be the same in the geometry and mapping folders.

Example of setup geometry for an ME0 stack prototype:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<setup label="ME0 stack 4 layers (April 2023)">

    <tracking>
        <trackSelection>bestChi2</trackSelection>
    </tracking>

    <detector id="0" label="ME0-II-CHAMBER-0002-proto" tracker="true">
        <geometry>me0</geometry>
        <alignment>
            <x>0.0</x>
            <y>0.0</y>
            <z>0.0</z>
            <phi_z>0.0</phi_z>
        </alignment>
    </detector>
    ...
</setup>
```

For each detector in the setup geometry file, the `id` parameter must be the same as the `chamber` column in the setup mapping file.

The detector geometries are stored in the `geometry/detectors` folder; the name of the detector geometry file must be the same as the `geometry` field in the setup geometry file.

Example of detector geometry for an ME0 module:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<detectorGeometry label="me0">
    <geometry>
        <baseSmall>235.2</baseSmall>
        <baseLarge>574.8</baseLarge>
        <height>787.9</height>
    </geometry>
    <readout type="strips">
        <nEta>8</nEta>
        <nStrips>384</nStrips>
    </readout>
</detectorGeometry>
```

!!! bug
    The geometry does not yet support eta partitions with different heights in the same detector.

## Supported setups

A list of the presently supported detector geometries and mappings follows:

| Setup                             | Geometry name        |
| --------------------------------- | -------------------- |
| CMS GEM test beam 2021            | `oct2021`            |
| CMS GEM test beam May 2022        | `may2022`            |
| CMS ME0 GIF++ test beam July 2022 | `july2022`           |
| ME0 stack (4 layers)              | `stack-tb-april2023` |
| MPGD-HCAL at 2022 SPS test beam   | `hcal-july2023`      |
| MPGD-HCAL at 2022 PS test beam    | `hcal-aug2023`       |
| GE2/1 QC8 with front detectors    | `qc8-front`          |
| GE2/1 QC8 with back detectors     | `qc8-back`           |

The geometry name has to be passed to each reconstruction step using the `--geometry GEOMETRY-NAME` command line argument.

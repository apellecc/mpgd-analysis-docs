# Reconstruction

Reconstruction consists of converting the binary data obtained from the detector electronics to physical objects.

A typical track-based reconstruction chain in `mpgd-analysis` consists of the following steps, one after another:

- **unpacking**: decoding binary electronics data to *digi*, i.e. informations on readout channels;
- **hit reconstruction**: clustering hits in readout elements and mapping them to the detector geometry to obtain reconstructed hits, or *rechits*;
- **track reconstruction**: fitting a particle track from rechits to obtain a segment.

More in general, the reconstruction steps and their order depend on the detector purpose (e.g. muon tracking, calorimetry, timing etc.).
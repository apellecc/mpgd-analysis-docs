# Tracking

The tracking reconstruction step fits particle tracks using a list of selected tracking detectors and extrapolates them to all the other detectors to obtain propagated hits or *prophits*.

!!! note
    At the moment the reconstruction of only one track per event is supported.

Tracking detectors are identified by the attribute `tracker=true` in the [setup geometry](/reconstruction/geometry).
There are no constraints on the number of tracking detectors in the setup.

If N is the number of tracking detectors, the track reconstruction is performed N+1 times for each event:

- the first N times, in turn one tracking detector is excluded from the track reconstruction; the track is built using the remaining ones and extrapolated to the excluded detector. The track obtained is called *partial track* and the extrapolatedmail hits are called *partial prophits*[^1];
- the N+1th time, the track is built using al the tracking detectors and extrapolated to all the non-tracking ones.

[^1]: Partial tracks are useful to commission the tracker, e.g. measuring its space resolution or performing its alignment, or in cases where all the detectors are tracking detectors, for example in the QC8.

## Track reconstruction

The track itself is fitted using a linear function. In presence of multiple hits per layer along the z direction (noise or background), all possible track combinations are built and the track with the lowest $\chi^2$ is chosen as the best one.

## How to run

```bash
rechit-to-track RECHIT-FILE-NAME.root TRACK-FILE-NAME.root --geometry GEOMETRY-NAME 
```
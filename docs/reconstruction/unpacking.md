# Unpacking

The GEM unpacker decodes binary run data and saves them them to a *digi* ROOT file. An event in the digi format contains the position of each readout element that received a signal (defined by its chamber, readout (eta) partition and strip or pad number).

The GEM unpacker supports the `v302a` data format with the `AMCEvent` event type. For details on the data format have a look at the [data format specifications](/resources/data_formats.xlsx).

Streaming decompression - through the `zstd` algorithm used by the GEM local DAQ - is supported, which means the unpacker can take compressed files as an input. For setups that have multiple backend boards (or FEDs), such as QC8, the unpacker takes as an input the a list of raw files (one per FED) and merges them event by events.

The following run types are supported:

- **physics**: regular physics run;
- **latency scan**: the latency parameter is increased by a fixed amount at regular time intervals and its value is stored in the "Run parameter" block in the GEM payload trailer;
- **fake multi-BX readout**: at each L1A, the backend sends N consecutive L1As; the unpacker groups the N consecutive events in mega-events, so the resulting digi event contains also information about the digi time in BX units (not available in regular physics runs).

The run type is determined by the unpacker by reading out the binary file header.

## How to run

```bash
raw-to-digi --input RAW-FILE-NAME.raw(.zst) --output DIGI-FILE-NAME.root --geometry GEOMETRY-NAME 
```

!!! info
    Go back to the [mapping and geometry documentation](geometry.md) if you need to know which geometry name to use.